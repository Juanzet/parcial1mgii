﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ChangeLevel03 : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Player")
        {
            SceneManager.LoadScene("Scene03GP");
        }
    }
}
