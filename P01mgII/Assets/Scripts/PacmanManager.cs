﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PacmanManager : MonoBehaviour
{
    [SerializeField] private Text txtPacman;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Player")
        {
            txtPacman.text = "Con la tecla Ctrl Izquierda, activas el shield";
        } else
        {
            txtPacman.text = " ";
        }
    }
}
