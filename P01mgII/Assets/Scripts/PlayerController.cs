﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float forceJump;
    public float forceDash;
    //private Rigidbody rb;
   
    void Start()
    {
      //rb = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * forceJump * Time.deltaTime;
        }

        if(Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += Vector3.right * forceDash * Time.deltaTime;
        }


        float h = Input.GetAxis("Horizontal");

        Vector3 vector = new Vector3(h, 0f, 0f);

        transform.Translate(vector * speed * Time.deltaTime);
    }

}
