﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TextInstanciate : MonoBehaviour
{

    public Text textLevel01;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        { 
            textLevel01.text = "Si mantienes la W presionada, saltaras mas alto";
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textLevel01.text = " ";
        }
    }
}
