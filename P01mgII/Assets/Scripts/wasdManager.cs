﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class wasdManager : MonoBehaviour
{
    [SerializeField] private Text txtWASD;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Player")
        {
            txtWASD.text = "Con las teclas A  y D = Te moves de izquierda a derecha";
        } else
        {
            txtWASD.text = " ";
        }
    }
}
