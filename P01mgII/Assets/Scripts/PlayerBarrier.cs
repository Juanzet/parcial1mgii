﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBarrier : MonoBehaviour
{
    [SerializeField] private BoxCollider barrier;
     private Color colorNormal = Color.white;
     private Color colorBarrier = Color.blue;
     private Renderer rend;

    void Awake()
    {
        rend = GetComponent<Renderer>();       
    }
    void Update()
    {
        BarrierLogic();
    }

    private void BarrierLogic()
    {
        if(Input.GetKey(KeyCode.LeftControl))
        {
            rend.material.color = colorBarrier;
            barrier.isTrigger = true;
            barrier.enabled = true;
        } else
        {
            rend.material.color = colorNormal;
            barrier.enabled = false;
            barrier.isTrigger = false;
        }
    }
}
