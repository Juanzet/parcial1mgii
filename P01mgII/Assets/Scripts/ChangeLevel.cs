﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ChangeLevel : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {   
        if(other.gameObject.CompareTag("Player"))
            SceneManager.LoadScene("Scene02GP");
    }
}
