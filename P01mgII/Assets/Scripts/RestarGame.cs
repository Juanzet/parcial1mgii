﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RestarGame : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag==("Player"))
        {
            SceneManager.LoadScene("Scene01GP");
        }
    }
}
