﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class WinScene : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Player")
        {
            SceneManager.LoadScene("Win");
        }
    }
}
