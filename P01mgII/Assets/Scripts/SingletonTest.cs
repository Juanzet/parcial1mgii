﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonTest : MonoBehaviour
{
    public static SingletonTest instance;

    [SerializeField] private AudioSource audioSource;
    
    private void Awake()
    {
        if(SingletonTest.instance == null)
        {
            SingletonTest.instance = this;
            DontDestroyOnLoad(this.gameObject);
            audioSource.playOnAwake = true;
        } else
        {
            Destroy(gameObject);
        }
    }

    

   
}
