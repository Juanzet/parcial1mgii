﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public Transform player;
    public float speed;

    
    void Update()
    {
        transform.LookAt(player);
        Vector3 directionToFollow = player.position - transform.position;
        transform.Translate(directionToFollow * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("Scene01GP");
        }
    }
}
