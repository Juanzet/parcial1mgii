﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptLatido : MonoBehaviour
{
    public float formaOriginal;
    public float sen;
    public float divisor;

    void Update()
    {
        float animacion = formaOriginal + Mathf.Sin(Time.time * sen) * formaOriginal / divisor;
        transform.localScale = Vector3.one * animacion;
    }
}
