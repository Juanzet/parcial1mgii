﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ChangeLevel02 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
         if(other.gameObject.CompareTag("Player"))
         {
            SceneManager.LoadScene("Scene03GP");
         }
    }
}
