﻿using UnityEngine.UI;
using UnityEngine;

public class TextInstanciate02 : MonoBehaviour
{
    public Text textLevel02;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textLevel02.text = "Presiona Shift para impulstarte en el aire";
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textLevel02.text = " ";
        }
    }
}
