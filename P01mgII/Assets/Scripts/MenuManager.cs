﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public void QuitAplication()
    {
        Application.Quit();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Scene01GP");
    }
}
