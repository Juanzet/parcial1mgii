I can’t see
Even with my glasses on

With all this information in front of me
Something must be wrong

Some days, I could be the last man for you
Give myself up to you
And get lost in your dreams

I cannot breathe
Sometimes through the smoke
This hazy fog that feels my dreams
The city’s where I breathe
The city’s where I breathe

Some days, I could be the last man for you
Give myself up to you
And get lost in your dreams

/Espanho/
No puedo ver
Incluso con las gafas puestas

Con toda esta información frente a mí
Algo debe estar mal

Algunos días, podría ser el último hombre para ti
Entregarme a ti
Y perderme en tus sueños

No puedo respirar
A veces a través del humo
Esta niebla que siente mis sueños
La ciudad es donde respiro
La ciudad es donde respiro

Algunos días, podría ser el último hombre para ti
Entregarme a ti
Y perderme en tus sueños